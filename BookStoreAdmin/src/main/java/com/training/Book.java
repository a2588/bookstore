package com.training;

public class Book {

	private String isbn;
	private String title;
	private Double price;
	private String category;
	private long stock;


	public Book() {
		super();
	}
	public Book(String isbn, String title, Double price, String category, long stock) {
		super();
		this.isbn = isbn;
		this.title = title;
		this.price = price;
		this.category = category;
		this.stock = stock;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public long getStock() {
		return stock;
	}
	public void setStock(long stock) {
		this.stock = stock;
	}

	public String toString()
	{
		return isbn+" "+title+" "+price+" "+category+" "+stock;
	}

}
