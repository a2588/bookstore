package com.training;

import java.util.List;

import com.training.services.CustomerService;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/customer")

public class CustomerResource {
	
	private CustomerService customer=new CustomerService();
	
	public String welcomeCustomer()
	{
		return "Welcome to Customer Page!";
	}
	
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	@Path("/getallbooks")
	public List<Book> getAllBooks()
	{
		return customer.getAllBook();

	}
	
	
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	@Path("/searchbook")
	public List<Book> searchPattern(String title)
	{
		return customer.searchPattern(title);
	}
	
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	@Path("/buybook")
	public int buysbook(String isbn, Integer cusId)
	{
		return customer.buys(isbn, cusId);
	}
	
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	@Path("/getbook")
	public List<Book> getBook(String category)
	{
		return customer.getBook(category);
	}


}
