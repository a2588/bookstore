package com.training;

public class Customer {

	private Integer cusId;
	private String name;
	public Customer() {
		super();
	}
	public Customer(Integer cusId, String name) {
		super();
		this.cusId = cusId;
		this.name = name;
	}
	public Integer getcusId() {
		return cusId;
	}
	public void setcusId(Integer cusId) {
		this.cusId = cusId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String toString()
	{
		return cusId+" "+name;
	}

}
