package com.training.services;

import java.util.List;

import com.training.Book;
import com.training.BookDao;

public class CustomerService {

	Book book = new Book();
	BookDao bookDao = new BookDao();

	public int buys(String isbn, Integer cusId)
	{
		List<Book> bookList = bookDao.getAllBook();
		for(Book b:bookList)
		{
			if(b.getIsbn().equals(isbn) && b.getStock()>0)
			{
				bookDao.updateStock(isbn);
				bookDao.addPurchasedBook(cusId, isbn);
				return 1;
			}
		}
		return -1;
		
	}

	public List<Book> getAllBook() 
	{
		return bookDao.getAllBook();
	}

	public List<Book> searchPattern(String title) 
	{
		return bookDao.searchPattern(title);
	}

	public List<Book> getBook(String category)
	{
		return bookDao.getBook(category);
	}
}
