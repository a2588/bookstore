package com.training.services;

import java.util.List;

import com.training.BookDao;
import com.training.Book;

public class AdminService {

	private BookDao bookDao=new BookDao();

	public Book addBook(Book book)
	{
		if(bookDao.addBook(book) > 0)
			return book;
		return null;
	}

	public List<Book> getAllBook() {
		return bookDao.getAllBook();
	}

	public int deleteBook(String isbn) {
		return bookDao.deleteBook(isbn);
	}

}
