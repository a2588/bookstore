package com.training;

import java.util.List;

import com.training.services.AdminService;

import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/admin")
public class AdminResource {

	private AdminService admin = new AdminService();


	@GET
	public String welcomeAdmin()
	{
		return "Welcome to Admin Page";
	}

	@Produces(MediaType.APPLICATION_JSON)
	@GET
	@Path("/getbooks")
	public List<Book> getBooks()
	{
		return admin.getAllBook();

	}

	@Produces(MediaType.APPLICATION_JSON)
	@POST
	@Path("/addbook")
	public Book addBook(Book book)
	{

		return admin.addBook(book);

	}

	@DELETE
	@Path("deletebook/isbn/{isbn}")
	public int deleteEmployee(@PathParam("isbn") String isbn)
	{

		return admin.deleteBook(isbn);
	}

}
