package com.training;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BookDao {

	private Connection connection;
	public BookDao()
	{

		connection = new DbConnection().getConnection();
		System.out.println(connection);
		if(connection != null)
			System.out.println("Ping Successful..");
	}

	public int addBook(Book book)
	{
		//Step 3
		String template = "insert into book values(?,?,?,?,?)";
		try(PreparedStatement pstmt = connection.prepareStatement(template)) {

			pstmt.setString(1, book.getIsbn());
			pstmt.setString(2, book.getTitle());
			pstmt.setDouble(3, book.getPrice());
			pstmt.setString(4, book.getCategory());
			pstmt.setLong(5, book.getStock());

			return pstmt.executeUpdate();

		}catch(SQLException e)
		{
			e.printStackTrace(); return -1;
		}

	}

	public List<Book> getAllBook() {
		String template = "select * from book";
		List<Book> bookList=new ArrayList<>();
		try(Statement stmt = connection.createStatement()) {
			ResultSet rs = stmt.executeQuery(template);
			while(rs.next()) {
				Book book = new Book();
				book.setIsbn(rs.getString(1));
				book.setTitle(rs.getString(2));
				book.setPrice(rs.getDouble(3));
				book.setCategory(rs.getString(4));
				book.setStock(rs.getLong(5));
				bookList.add(book);
			}
			return bookList;
		} catch(SQLException e) {
			e.printStackTrace();
			return bookList;
		}
	}

	public List<Book> getBook(String category)
	{
		String template = "select * from book where category=?";
		List<Book> bookListCategory=new ArrayList<>();
		try(PreparedStatement pstmt = connection.prepareStatement(template))
		{
			pstmt.setString(1, category);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next())
			{
				Book book=new Book();
				book.setIsbn(rs.getString(1));
				book.setTitle(rs.getString(2));
				book.setPrice(rs.getDouble(3));
				book.setCategory(rs.getString(4));
				book.setStock(rs.getLong(5));
				bookListCategory.add(book);
			}
			return bookListCategory;	

		}
		catch(SQLException e)
		{
			e.printStackTrace();return null;
		}
	}

	public int deleteBook(String isbn) {
		String template = "delete from book where isbn=?";
		try(PreparedStatement pstmt = connection.prepareStatement(template)) {
			pstmt.setString(1,isbn);
			return pstmt.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int updateStock(String isbn) {
		String template = "update book set stock=stock-1 where isbn=?";
		try(PreparedStatement pstmt = connection.prepareStatement(template)) {
			pstmt.setString(1,isbn);
			return pstmt.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public List<Book> searchPattern(String title) {

		List<Book> bookSearchList=new ArrayList<>();
		String template = "select * from book where title like '%?%'";
		try(PreparedStatement pstmt = connection.prepareStatement(template))
		{
			pstmt.setString(1,"%"+title+"%");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next())
			{
				Book book=new Book();
				book.setIsbn(rs.getString(1));
				book.setTitle(rs.getString(2));
				book.setPrice(rs.getDouble(3));
				book.setCategory(rs.getString(4));
				book.setStock(rs.getLong(5));
				bookSearchList.add(book);
			}
			return bookSearchList;	

		}
		catch(SQLException e)
		{
			e.printStackTrace();return null;
		}
	}

	public int addPurchasedBook(Integer cusId, String isbn)
	{
		String template = "insert into purchased_book (cusID,isbn) values (?,?)";
		try(PreparedStatement pstmt = connection.prepareStatement(template)) {

			pstmt.setInt(1, cusId);
			pstmt.setString(2, isbn);

			return pstmt.executeUpdate();

		}catch(SQLException e)
		{
			e.printStackTrace(); return -1;
		}

	}

}




